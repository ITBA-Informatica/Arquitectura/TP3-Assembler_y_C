section .text


global sys_write

sys_write:
	push ebp
	mov ebp,esp
	and	esp, -16
	pushad



	;sys_write(STDOUT, (void *) str, len);
	mov ebx,[ebp+8]
	mov eax,4
	mov edx,[ebp+16]
	mov ecx, [ebp+12]
	int 80h

	popad
	mov esp,ebp
	pop ebp
	ret