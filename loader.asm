section .text

global _start
extern exit
extern main

_start:

push ebp
mov ebp, esp

sub esp, 8
mov eax,[ebp+4]
mov [esp],eax

mov eax,ebp
add eax,8
mov [esp+4], eax
call main
call exit
