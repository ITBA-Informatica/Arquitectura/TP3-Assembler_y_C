section .text


GLOBAL main
EXTERN print
; EXTERN printf
; EXTERN puts
EXTERN exit

main:
	push ebp
	mov ebp,esp
	pushad
	push esi
	push edi
	mov ebx,[ebp+8] 	;argC
	mov [argNumber],ebx
	xor edx,edx
	mov [argNumber+4],edx
	call print
	call exit
	push ebx
	call print
	push ebx
	mov esi, ebx
	mov edi, 0
	; push fmt
	; call printf
	; add esp,8
ciclo:
	cmp esi,0
	jz return
	mov eax,[ebp+12] ; char*
	mov ebx,[eax+edi*4]
	mov eax,edi
	inc eax
	push ebx
	push eax
	call print
	; push fmt2
	; call printf
	add esp,12
	inc edi
	dec esi
	jmp ciclo
return:
	pop edi
	pop esi	
	popad
	mov esp,ebp
	pop ebp
	ret


section .bss
argNumber resb 64

section .rodata
fmt db `Cantidad de argumentos??:  %d. \n`, 0
fmt2 db `Argumento %d: %s \n`, 0
